package com.example.actors.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;

import com.example.actors.app.Formulas;
import com.example.actors.app.FormulasMatrix;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
	    System.out.println("Czy film ma by� popularny i kasowy(Y/N)?");
	    String movieHasToBePopular = bufferRead.readLine();
	    System.out.println("Czy film ma mie� uznanie w�r�d krytyk�w(Y/N)?");
	    String movieHasToGetHighRating = bufferRead.readLine();
	    System.out.println("Czy film ma mie� wysoki bud�et(Y/N)?");
	    String movieWillHaveBigBudget = bufferRead.readLine();
		Formulas formulas = prepareFormulas(movieHasToBePopular,
				movieHasToGetHighRating, movieWillHaveBigBudget);
	    FormulasMatrix matrix = new FormulasMatrix(formulas, 16, 2, 3, 18);
	    Set<String> results = matrix.calculateMatrix();
	    
	    System.out.println("Do Twojego wyboru najlepiej pasuj� aktorzy: ");
	    for (String actor : results) {
			System.out.println(actor);
		}
	}

	private static Formulas prepareFormulas(String movieHasToBePopular,
			String movieHasToGetHighRating, String movieWillHaveGoodBoxOffice) {
		Formulas formulas = new Formulas();

		formulas.setMovieHasToBePopular(castToBoolean(movieHasToBePopular));
		formulas.setMovieHasToGetHighRating(castToBoolean(movieHasToGetHighRating));
		formulas.setMovieWillHaveBigBudget(castToBoolean(movieWillHaveGoodBoxOffice));
		return formulas;
	}

	private static boolean castToBoolean(String hasOscar) {
		if ("Y".equals(hasOscar)) {
			return true;
		}
		return false;
		
	}

}
