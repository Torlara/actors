package com.example.actors.app;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Set;

/**
 * Created by boba on 2014-05-25.
 */
public class Result extends Activity {

    Button buttonRestart;

    ImageView actorPhoto;

    TextView actorName;
    TextView actorDesc;

    Boolean movieHasToBePopular;
    Boolean movieHasToGetHighRating;
    Boolean movieWillHaveBigBudget;

    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_layout);

        buttonRestart = (Button) findViewById(R.id.buttonRestart);

        actorPhoto = (ImageView) findViewById(R.id.actorPhoto);
        actorName = (TextView) findViewById(R.id.actorName);
        actorDesc = (TextView) findViewById(R.id.actorDesc);

        preferences = getSharedPreferences("preferences", 0);
        movieHasToBePopular = preferences.getBoolean("movieHasToBePopular", Boolean.parseBoolean(null));
        movieWillHaveBigBudget = preferences.getBoolean("movieWillHaveHighBudget", Boolean.parseBoolean(null));
        movieHasToGetHighRating = preferences.getBoolean("movieHasToGetHighRanking", Boolean.parseBoolean(null));


        //Toast.makeText(getApplicationContext(), "popular: " + movieHasToBePopular, Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(), "budget: " + movieWillHaveBigBudget, Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(), "rate: " + movieHasToGetHighRating, Toast.LENGTH_SHORT).show();

        getActor();


        buttonRestart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getActors() throws IOException {
        Formulas formulas = new Formulas();
        formulas.setMovieHasToBePopular(movieHasToBePopular);
        formulas.setMovieHasToGetHighRating(movieHasToGetHighRating);
        formulas.setMovieWillHaveBigBudget(movieWillHaveBigBudget);
        FormulasMatrix matrix = new FormulasMatrix(formulas, 16, 2, 3, 18);
        Set<String> results = matrix.calculateMatrix();

    }

    public void getActor(){
        if(!movieHasToBePopular && movieHasToGetHighRating && !movieWillHaveBigBudget){
            actorPhoto.setImageResource(R.drawable.adrien_brody);
            actorName.setText("Adrien Brody");
            actorDesc.setText("(ur. 14 kwietnia 1973 w Nowym Jorku) – amerykański aktor filmowy. Jego rola Władysława Szpilmana w biograficznym dramacie wojennym Romana Polańskiego Pianista (2002) została nagrodzona Oscarem (był najmłodszym laureatem w historii kina)");
        }
        else if(!movieHasToBePopular && movieHasToGetHighRating && movieWillHaveBigBudget){
            actorPhoto.setImageResource(R.drawable.robert_de_niro);
            actorName.setText("Robert de Niro");
            actorDesc.setText("(ur. 17 sierpnia 1943 w Nowym Jorku) – amerykański aktor, reżyser i producent, zdobywca dwóch Nagród Akademii Filmowej za role w filmach Ojciec chrzestny II (1974) oraz Wściekły byk (1980).");
        }
        else if(movieHasToBePopular && !movieHasToGetHighRating && !movieWillHaveBigBudget){
            actorPhoto.setImageResource(R.drawable.jean_reno);
            actorName.setText("Jean Reno");
            actorDesc.setText("(ur. 30 lipca 1948 w Casablance) – aktor francuski, hiszpańskiego pochodzenia. Swoją popularność zawdzięcza Lucowi Bessonowi, udział w takich filmach jak: \"Wielki błękit\", \"Nikita\", czy \"Leon zawodowiec\" uczyniły z aktora międzynarodową sławę");
        }
        else if(movieHasToBePopular && !movieHasToGetHighRating && movieWillHaveBigBudget){
            actorPhoto.setImageResource(R.drawable.bruce_willis);
            actorName.setText("Bruce Willis");
            actorDesc.setText("(ur. 19 marca 1955 w Idar-Oberstein) – amerykański aktor i producent filmowy. Bruce uważany jest za jednego z najbardziej kasowych aktorów Hollywood. W 1988 roku wystąpił w obrazie, który uczynił z niego międzynarodową gwiazdę - \"Szklanej pułapce\".");
        }
        else if(movieHasToBePopular && movieHasToGetHighRating && movieWillHaveBigBudget){
            actorPhoto.setImageResource(R.drawable.christian_bale);
            actorName.setText("Christian Bale");
            actorDesc.setText("(ur. 30 stycznia 1974 w Pembrokeshire) − brytyjski aktor filmowy i telewizyjny, producent filmowy. Laureat Oscara za drugoplanową rolę męską w filmie Fighter w 2010 roku.");
        }
        else if(movieHasToBePopular && movieHasToGetHighRating && !movieWillHaveBigBudget){
            actorPhoto.setImageResource(R.drawable.bill_murray);
            actorName.setText("Bill Murray");
            actorDesc.setText("(ur. 21 września 1950 w Wilmette w stanie Illinois) – amerykański aktor filmowy, pisarz, komik. Nominowany do Oscara za pierwszoplanową rolę w filmie Między słowami (2003) w reżyserii Sofii Coppoli.");
        }
        else if(!movieHasToBePopular && !movieHasToGetHighRating && !movieWillHaveBigBudget){
            actorPhoto.setImageResource(R.drawable.mads_mikkelsen);
            actorName.setText("Mads Mikkelsen");
            actorDesc.setText("(ur. 22 listopada 1965 roku w Østerbro, na obszarze Kopenhagi) – duński aktor filmowy, najbardziej znany jako czarny charakter w nowym głośnym serialu NBC \"Hannibal\", w którym gra tytułową rolę.");
        }
        else if(!movieHasToBePopular && !movieHasToGetHighRating && movieWillHaveBigBudget){
            actorPhoto.setImageResource(R.drawable.benedict_cumberbatch);
            actorName.setText("Benedict Cumberbatch");
            actorDesc.setText("(ur. 19 lipca 1976 w Londynie) – brytyjski aktor filmowy, telewizyjny i teatralny oraz producent. Jego najbardziej znaną rolą jest współczesne wcielenie Sherlocka Holmesa w serialu Sherlock (2010) wyprodukowanym przez stację BBC.");
        }
        else {
            actorPhoto.setImageResource(R.drawable.actor);
            actorName.setText("Nie znaleziono");
        }
    }
}