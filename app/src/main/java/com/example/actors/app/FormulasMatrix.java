package com.example.actors.app;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import org.apache.commons.lang3.StringUtils;


public class FormulasMatrix {

    private Formulas formulas;

    private int nrOfActors;
    private int nrOfAdditions;
    private int nrOfResults;
    private int nrOfFacts;

    private int powNrOfActors;
    private int powNrOfAdditions;
    private int powNrOfResults;
    private int powNrOfFacts;

    private boolean[][] actors;
    private boolean[][] additions;
    private boolean[][] facts;
    private boolean[][] results;

    private boolean[] factsImplications;
    private boolean[] factOut;
    private boolean[][] factsConjunction;
    Set<Integer> s1 = new HashSet<Integer>();
    Set<Integer> s2 = new HashSet<Integer>();
    Set<String> actorsResult = new HashSet<String>();


    public FormulasMatrix(final Formulas formulas, int nrOfActors, int nrOfAdditions,
                          int nrOfResults, int nrOfFacts) {
        this.formulas = formulas;
        this.nrOfActors = nrOfActors;
        this.powNrOfActors = (int)Math.pow(2, nrOfActors);
        this.actors = new boolean[powNrOfActors][nrOfActors];
        this.nrOfAdditions = nrOfAdditions;
        this.powNrOfAdditions = (int)Math.pow(2, nrOfAdditions);
        this.additions = new boolean[powNrOfAdditions][nrOfAdditions];
        this.nrOfResults = nrOfResults;
        this.powNrOfResults = (int)Math.pow(2, nrOfResults);
        this.results = new boolean[powNrOfResults][nrOfResults];
        this.nrOfFacts = nrOfFacts;
        this.powNrOfFacts = (int)Math.pow(2, nrOfActors + nrOfAdditions + nrOfResults);
        this.facts = new boolean[powNrOfFacts][this.nrOfFacts];
        this.factsImplications = new boolean[powNrOfFacts];
        this.factOut = new boolean[powNrOfFacts];
        this.factsConjunction = new boolean[powNrOfFacts][2];
    }

    public Set<String> calculateMatrix() {
        prepareActorsMatrix();
        prepareAdditions();
        prepareResults();
        prepareFacts();
        prepareFactsImpls();
        prepareFactsConjunction();
        s1.removeAll(s2);
        for (Integer index : s1) {
            boolean[] actorCombination = actors[index];
            int searchIndex = 0;
            for (boolean actor : actorCombination) {
                if (actor) {
                    actorsResult.add(getCurrentActor(searchIndex));
                }
                searchIndex++;
            }
        }
        return actorsResult;

    }

    private String getCurrentActor(int searchIndex) {
        switch(searchIndex) {
            case 0: return Formulas.ACTOR_1;
            case 1: return Formulas.ACTOR_2;
            case 2: return Formulas.ACTOR_3;
            case 3: return Formulas.ACTOR_4;
            case 4: return Formulas.ACTOR_5;
            case 5: return Formulas.ACTOR_6;
            case 6: return Formulas.ACTOR_7;
            case 7: return Formulas.ACTOR_8;
            case 8: return Formulas.ACTOR_9;
            case 9: return Formulas.ACTOR_10;
            case 10: return Formulas.ACTOR_11;
            case 11: return Formulas.ACTOR_12;
            case 12: return Formulas.ACTOR_13;
            case 13: return Formulas.ACTOR_14;
            case 14: return Formulas.ACTOR_15;
            case 15: return Formulas.ACTOR_16;
        }
        return null;
    }

    private void prepareFactsConjunction() {
        for(int i = 0; i<powNrOfFacts; i++) {
            factsConjunction[i][0] = factsImplications[i] && factOut[i];
            if (factsConjunction[i][0]) {
                s1.add(i/(powNrOfFacts/powNrOfActors));
            }
            factsConjunction[i][1] = factsImplications[i] && !factOut[i];
            if (factsConjunction[i][1]) {
                s2.add(i/(powNrOfFacts/powNrOfActors));
            }
        }
    }

    private boolean determineBudget(int i) {
        return formulas.isMovieWillHaveBigBudget() ? results[i][2] : !results[i][2];
    }

    private boolean determineRating(int i) {
        return formulas.isMovieHasToGetHighRating() ? results[i][1] : !results[i][1];
    }

    private boolean determinePopular(int i) {
        return formulas.isMovieHasToBePopular() ? results[i][0] : !results[i][0];
    }

    private void prepareFactsImpls() {
        for (int i = 0; i<powNrOfFacts; i++) {
            factsImplications[i] = facts[i][0] && facts[i][1] && facts[i][2]
                    && facts[i][3] && facts[i][4] ;
//                    && facts[i][5] && facts[i][6]
//                    && facts[i][7] && facts[i][8] && facts[i][9]
//                    && facts[i][10] && facts[i][11] ;
//							&& facts[i][12]
//					&& facts[i][13] && facts[i][14] && facts[i][15]
//					&& facts[i][16] && facts[i][17];
        }
    }

    private void prepareFacts() {
        int rowCnt = 0;
        for (int i = 0; i < powNrOfActors; i++) {
            for (int j = 0; j < powNrOfAdditions; j++) {
                for (int k = 0; k < powNrOfResults; k++) {
                    boolean[] actorRow = actors[i];
                    boolean[] additionsRow = additions[j];
                    boolean[] resultsRow = results[k];
                    facts[rowCnt][0] = implies(actorRow[0], !additionsRow[0]
                            && !additionsRow[1] && !resultsRow[0]
                            && resultsRow[1] && !resultsRow[2]);
                    facts[rowCnt][1] = implies(actorRow[1], !additionsRow[0]
                            && !additionsRow[1] && !resultsRow[0]
                            && resultsRow[1] && resultsRow[2]);
                    facts[rowCnt][2] = implies(actorRow[2], !additionsRow[0]
                            && !additionsRow[1] && resultsRow[0]
                            && !resultsRow[1] && !resultsRow[2]);
                    facts[rowCnt][3] = implies(actorRow[3], !additionsRow[0]
                            && !additionsRow[1] && resultsRow[0]
                            && !resultsRow[1] && resultsRow[2]);
                    facts[rowCnt][4] = implies(actorRow[4], !additionsRow[0]
                            && !additionsRow[1] && resultsRow[0]
                            && resultsRow[1] && resultsRow[2]);
//                    facts[rowCnt][5] = implies(actorRow[5], !additionsRow[0]
//                            && additionsRow[1] && !resultsRow[0]
//                            && resultsRow[1] && !resultsRow[2]);
//                    facts[rowCnt][6] = implies(actorRow[6], !additionsRow[0]
//                            && additionsRow[1] && !resultsRow[0]
//                            && resultsRow[1] && resultsRow[2]);
//                    facts[rowCnt][7] = implies(actorRow[7], !additionsRow[0]
//                            && additionsRow[1] && resultsRow[0]
//                            && !resultsRow[1] && !resultsRow[2]);
//                    facts[rowCnt][8] = implies(actorRow[8], !additionsRow[0]
//                            && additionsRow[1] && resultsRow[0]
//                            && !resultsRow[1] && resultsRow[2]);
//                    facts[rowCnt][9] = implies(actorRow[9], !additionsRow[0]
//                            && additionsRow[1] && resultsRow[0]
//                            && resultsRow[1] && !resultsRow[2]);
//					facts[rowCnt][10] = implies(actorRow[10], !additionsRow[0]
//							&& additionsRow[1] && resultsRow[0]
//							&& resultsRow[1] && resultsRow[2]);
//					facts[rowCnt][11] = implies(actorRow[11], !additionsRow[0]
//							&& additionsRow[1] && !resultsRow[0]
//							&& resultsRow[1] && !resultsRow[2]);
//					facts[rowCnt][12] = implies(actorRow[12], additionsRow[0]
//							&& additionsRow[1] && !resultsRow[0]
//							&& resultsRow[1] && resultsRow[2]);
//					facts[rowCnt][13] = implies(actorRow[13], additionsRow[0]
//							&& additionsRow[1] && resultsRow[0]
//							&& !resultsRow[1] && resultsRow[2]);
//					facts[rowCnt][14] = implies(actorRow[14], additionsRow[0]
//							&& additionsRow[1] && resultsRow[0]
//							&& resultsRow[1] && !resultsRow[2]);
//					facts[rowCnt][15] = implies(actorRow[15], additionsRow[0]
//							&& additionsRow[1] && resultsRow[0]
//							&& resultsRow[1] && resultsRow[2]);

                    facts[rowCnt][10] = implies(additionsRow[0]
                            && additionsRow[1], resultsRow[0] && resultsRow[1]);
                    facts[rowCnt][11] = implies(resultsRow[0] && resultsRow[1],
                            resultsRow[2]);
                    factOut[rowCnt] = determinePopular(k) && determineRating(k)
                            && determineBudget(k);
                    rowCnt++;
                }
            }
        }
    }

    private void prepareResults() {
        prepareAllCombinations(results, nrOfResults);
    }

    private boolean implies(boolean leftSide, boolean rightSide) {
        return leftSide ? (rightSide ? true : false) : true;
    }


    private void prepareAdditions() {
        prepareAllCombinations(additions, nrOfAdditions);
    }


    private void prepareActorsMatrix() {
        prepareAllCombinations(actors, nrOfActors);
    }

    private void prepareAllCombinations(boolean[][] matrix, int nrOfElements) {
        for (int i = 0; i < matrix.length; i++) {
            String padded = changeToBinaryAndPadWith0(i, nrOfElements);
            List<Integer> indexesOfTrue = new ArrayList<Integer>();
            Integer[] indexesOfTrueArray = findIndexesWith1(padded,
                    indexesOfTrue);
            fillRow(matrix, i, indexesOfTrueArray);
        }
    }

    private Integer[] findIndexesWith1(String padded,
                                       List<Integer> indexesOfTrue) {
        for (int j = 0; j<padded.length(); j++) {
            if (padded.charAt(j) == '1') {
                indexesOfTrue.add(j);
            }
        }
        Integer[] indexesOfTrueArray = new Integer[0];
        indexesOfTrueArray = indexesOfTrue.toArray(indexesOfTrueArray);
        return indexesOfTrueArray;
    }

    private String changeToBinaryAndPadWith0(int i, int nrOfElements) {
        String binaryRep = Integer.toBinaryString(i);
        String padded = StringUtils.leftPad(binaryRep, nrOfElements, "0");
        return padded;
    }

    private void fillRow(boolean[][] matrix, int row, Integer... trues) {
        for (int i : trues) {
            matrix[row][i] = true;
        }
    }

    public Formulas getFormulas() {
        return formulas;
    }

    public void setFormulas(Formulas formulas) {
        this.formulas = formulas;
    }

}
