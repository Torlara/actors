package com.example.actors.app;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * Created by boba on 2014-05-25.
 */
public class MovieQuestions extends Activity {


    RadioGroup radioCost;
    RadioGroup radioRank;
    RadioGroup radioPopularity;

    RadioButton radioPopular;

    Button buttonNext;

    SharedPreferences preferences;
    int idPopularity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_layout);

        preferences = getSharedPreferences("preferences",0);


        buttonNext=(Button)findViewById(R.id.buttonNext2);

        radioCost=(RadioGroup)findViewById(R.id.radioCost);
        radioRank=(RadioGroup)findViewById(R.id.radioRank);
        radioPopularity=(RadioGroup)findViewById(R.id.radioPopularity);

        final SharedPreferences.Editor editor = preferences.edit();
        editor.clear();

        //Czy film ma być poplarny i kasowy (T/N) ?
        radioPopularity.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radioPopular:
                        // do operations specific to this selection
                        editor.putBoolean("movieHasToBePopular",true);
                        editor.commit();
                        //Toast.makeText(getApplicationContext(), "wynik: " + checkedId, Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.radioAmbitious:
                        // do operations specific to this selection
                        editor.putBoolean("movieHasToBePopular",false);
                        editor.commit();
                        break;
                }


            }
        });

        //Czy film ma mieć wysokie uznanie krytyków(T/N) ?
        radioRank.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radioCritics:
                        // do operations specific to this selection
                        editor.putBoolean("movieHasToGetHighRanking",true);
                        editor.commit();
                        //Toast.makeText(getApplicationContext(), "wynik: " + checkedId, Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.radioPeople:
                        // do operations specific to this selection
                        editor.putBoolean("movieHasToGetHighRanking",false);
                        editor.commit();
                        break;
                }


            }
        });

        //Czy film ma mieć wysoki budzet(T/N) ?
        radioCost.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radioExpensive:
                        // do operations specific to this selection
                        editor.putBoolean("movieWillHaveHighBudget",true);
                        editor.commit();
                        //Toast.makeText(getApplicationContext(), "wynik: " + checkedId, Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.radioCheap:
                        // do operations specific to this selection
                        editor.putBoolean("movieWillHaveHighBudget",false);
                        editor.commit();
                        break;
                }


            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(),Result.class);
                startActivity(i);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}