package com.example.actors.app;

public class Formulas {

	public static String ACTOR_1 = "Benedict Cumberbatch";
	public static String ACTOR_2 = "Tom Hiddleston";
	public static String ACTOR_3 = "Chris Hemsworth";
	public static String ACTOR_4 = "Robert Pattison";
	public static String ACTOR_5 = "Bradley Cooper";
	public static String ACTOR_6 = "Jake Gyllenhaal";
	public static String ACTOR_7 = "Alan Rickman";
	public static String ACTOR_8 = "Jean Reno";
	public static String ACTOR_9 = "Bruce Willis";
	public static String ACTOR_10 = "Bill Murray";
	public static String ACTOR_11 = "Ben Affleck";
	public static String ACTOR_12 = "Adrien Brody";
	public static String ACTOR_13 = "Robert de Niro";
	public static String ACTOR_14 = "Colin Firth";
	public static String ACTOR_15 = "Javier Bardem";
	public static String ACTOR_16 = "Christian Bale";
	
	
	private boolean hasOscar;
	private boolean isExperienced;
    private boolean movieHasToBePopular;
    private boolean movieHasToGetHighRating;
    private boolean movieWillHaveBigBudget;
    
    
	public boolean isHasOscar() {
		return hasOscar;
	}
	public void setHasOscar(boolean hasOscar) {
		this.hasOscar = hasOscar;
	}
	public boolean isExperienced() {
		return isExperienced;
	}
	public void setExperienced(boolean isExperienced) {
		this.isExperienced = isExperienced;
	}
	public boolean isMovieHasToBePopular() {
		return movieHasToBePopular;
	}
	public void setMovieHasToBePopular(boolean movieHasToBePopular) {
		this.movieHasToBePopular = movieHasToBePopular;
	}
	public boolean isMovieHasToGetHighRating() {
		return movieHasToGetHighRating;
	}
	public void setMovieHasToGetHighRating(boolean movieHasToGetHighRating) {
		this.movieHasToGetHighRating = movieHasToGetHighRating;
	}
	public boolean isMovieWillHaveBigBudget() {
		return movieWillHaveBigBudget;
	}
	public void setMovieWillHaveBigBudget(boolean movieWillHaveGoodBoxOffice) {
		this.movieWillHaveBigBudget = movieWillHaveGoodBoxOffice;
	}
}
