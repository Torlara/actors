# Custom share

| Platform name | Custom | SDK level | Default share  | Confirmation | OAuth |
|---|---|---|---|---|---|
| [Facebook](https://developers.facebook.com/docs/sharing/overview)| yes | yes | yes | no* | yes* |
| [Facebook messenger](https://developers.facebook.com/docs/sharing/overview)| yes* | yes | yes | no* | yes* |
| [Twitter](https://docs.fabric.io/android/twitter/compose-tweets.html) | no* | no* | yes | yes | yes* |
| Sms | yes | yes* | yes | yes | no |
| [Email]() | yes | yes* | yes | yes* | yes* |
| [Instagram]() | | |
| [Snapchat]() | | | |
| [Vine]() | | | |
| [Tumblr]() | | | |
| [WhatsApp]() | | | |
| [Kik]() | | | |

##Facebook

1. Before you can share to Facebook from your app, you'll need to: 
	- Add the **Facebook SDK** for Android to your mobile development   
environment.
	- [Get a **Facebook App ID** properly configured](https://developers.facebook.com/docs/android/getting-started#app_id)  
and linked to your Android app (it means you should register your app   
as a Facebook Application in Facebook Developer profile).
	- [Generate an **Android Key Hash**](https://developers.facebook.com/docs/android/getting-started#create_hash)   
and add it to your developer profile.
	- Add **Facebook Activity** and include it in **AndroidManifest.xml**.
	- Set up a **ContentProvider** in your **AndroidManifest.xml**   
where {APP_ID} is the id of the Facebook application you've just created.  

2. **Confirmation if share was successful/unsuccessful**: Facebook dialogs   
have a `function(response)` that is called whenever the UI dialog is closed   
regardless of the cause (success/failure/cancel). You have to register  
**FacebookCallback<Sharer.Result>** and call the SDK's **callbackManager** in   
your **onActivityResult** to handle the response. The result does not tell us   
if the share was successful or not, it only indicates if the user has   
authorized the app to share content on their behalf.   
Also `onError(FacebookException)` is called when the dialog finishes with   
an error that occurred, but it might not give us expected information.

3. In general **OAuth** may be obtained from Graph API. To generate an app   
access token, you need to make a [Graph API call](https://developers.facebook.com/docs/facebook-login/access-tokens#apptokens).   
This call will return an app access token which can be used in as a   
a user access token to make API calls. However there are several types   
on access tokens to support different use cases. More to read [here](https://developers.facebook.com/docs/facebook-login/access-tokens).

###Content types to share
Each type of content users share has a class we can use to represent it.   
After modelling a content sharing interface should be added.
	
1. **Links**: Sharing links to Facebook includes attributes that show up in   
the post:
	- **contentUrl** - the link to be shared,
	- **contentTitle** - represent the title of the content in the link,
	- **imageUrl** - URL of thumbnail image that will appear on the post,
	- **contentDescription** - usually 2-4 sentences describing the content.
	- Share content should be built into the **ShareLinkContent** model.

2. **Photos**
	- The photos must be smaller than 12MB.
	- User needs to have the native Facebook for Android app installed.   
Version 7.0 or above is required.
	- Sharing can be handled with **Share Dialog** (SDK level) or   
**custom interface** (Custom).
	- Share content should be built into **SharePhotoContent** model.
	
3. **Videos**
	- Videos must be smaller than 12MB.
	- Sharing can be handled with **Share Dialog** (SDK level)   
or a **custom interface** (Custom).
	- Share content should be built into **ShareVideoContent** model.		
4. Users can also share stories from your app to Facebook through   
**Open Graph**. It can be also done via using **Share Dialog** or your   
own **custom interface**.

	- Before your app can publish Open Graph stories you'll need to:
		- Get a **Facebook App ID**, properly configured and linked to your   
Android app.
		- Add the **Facebook SDK for Android** to your project.
		- Add your **Facebook app ID**, **display name**, and **url scheme**   
to your app's **.plist file**.
		- Share content should be built into **ShareOpenGraphContent** model.
		
### Ways to share
1. The simplest way is to use **buttons** that trigger **Facebook dialogs**.   
They are provided by Facebook SDK and don't require implementing Facebook Login.   
(Default share)

2. **Share Dialog** is an easy way to let people share content without having   
them to log into your app or grant any permissions. The **Share dialog**   
switches to native Facebook for Android app (or WebView in case FB app is not  
 installed) and go back to the app just after sharing your content. (SDK level)

3. If you want to customize the sharing experience in the app, you can create   
a **custom sharing interface** (Custom). In this case your **custom dialog**   
needs to call the **Graph API**. Also you have to implement **Facebook Login**   
and you need to request the **publish_actions** permission, which requires   
Login Review. So to build your **own interface for sharing**, you need to:
	- Build a custom interface that posts to the   
`/me/feed` **Graph API endpoint**.
	- Implement **Facebook Login** in your app.
	- Request the **publish_actions permission** when people log into   
your app.
	- The code for posting a link to Facebook from your own interface:   
`ShareApi.share(content, null);`.
		
##Facebook messenger
1. Sharing via Facebook messenger is similar to sharing via the Facebook app.   
You can share it via buttons, native dialogs or a custom interface.

2. In order to use Messenger integration in the Facebook SDK, make sure you   
have imported **MessengerUtil** classes.

3. **Message dialog** allows you to share using the Facebook SDK. It lets   
users **privately share content** and like the **Share dialog** it's   
a **native sharing component** that doesn't require you to implement   
Facebook Login or request `publish_actions` permission.
	- The **Message dialog** enables users to share links, images and   
Open Graph stories. On mobile, the user must have the native   
Messenger app installed.
	
4. To share to Messenger you can use **Facebook SDK** or **share directly   
through Intents**. Facebook highly recommend using the SDK, as it provides   
a convenient API and correctly handles edge cases such as different   
versions of Messenger, as well as Messenger being absent on a phone.

5. As to **custom sharing interface** for Messenger, the documentation is a   
little unclear. It seems that it is also possible to create our own custom   
interface however it is not explicitly stated in Messenger documentation.

##Twitter

1. The **TweetComposer Kit** provides two ways to compose Tweets   
(default share):
	- **Twitter Application’s Tweet Composer** - a feature-rich composer which   
supports attaching images and videos. It allows the app to start the exported   
Tweet composer via an **Intent**. Then you have to build your Intent using   
**Builder**. If the Twitter app is not installed, the intent will launch   
twitter.com in a browser, but the specified image will be ignored.
	- **TwitterKit Tweet Composer** (Beta) - a lightweight composer that   
lets users compose Tweets with **App Cards** from within your application. 

2. To use TweetComposer Kit you have to:
	- Add the TweetComposer kit to your **build.gradle dependencies**.
	- Be sure the TweetComposer kit is included in your   
**application’s Fabric.with() call**.
	- **Authenticate** user with Twitter.
	- Build an **Intent** to start TweetComposer **ComposerActivity**   
and include **TwitterSession**.
	- If you want to attach an **Application Card** to a tweet, you should   
create a **Card** with the **AppCardBuilder**. For that a live Google Play   
Store Android package name is **required** for an App Card to be properly   
rendered in the Twitter App. It means you should specify a string value   
which is a representation of your app ID in   
Google Play (i.e. `com.android.app`).
	
	- App Cards have several requirements:
		- Valid image Uri to a local media file on the device.
		- Valid package name of a live Google Play application.
		- The card image size at least 800 pixels wide and 420 pixels tall.   
(Otherwise the card won’t be rendered in the Twitter Android and iOS apps.)
		- Recommended aspect ratio 1.91 tall. (It provides the best rendering,   
also other aspect ratios be cropped.)
		- Unique card image e.g. in-game screenshot or photo picker images.

3. **Confirmation if share was successful/unsuccessful**: After attempting to   
post a Tweet, the ComposerActivity **TweetUploadService** broadcasts an Intent   
with the action for success or for failure. On success, the Intent will   
contain an extra value with the **Tweet ID** of the created Tweet.   
On failure, the Intent will contain a copy of the original intent which could   
be used to retry the upload.

4. Sharing using **the Twitter SDK**, as opposed to using Intents is also not   
directly possible, as you still have to use an **Intent** which launches   
**Tweet Composer**. However you might slightly customize your tweet,   
for example by adding image, link or a card to your message.

5. **Custom sharing**: is not possible using only Twitter SDK. However there   
is a library called [Twitter4J](http://twitter4j.org/en/code-examples.html)   
which might be useful. It allows posting tweets, getting timeline, searching   
for tweets. Twitter4J is developed by its community members.   
**Important note:** Twitter4J even if it gives us the possibility to custom   
share shouldn't be used as a solution. It is extremely volatile as any   
change in Twitter API would render the library useless.   
(At least for some time until community won't adapt the library to   
new specifications.)

	With this library it is possible to post tweet through your own dialog.   
However it requires the user to log in with his Twitter account in your   
application. The overall steps needed to post custom tweet are:

	- User is asked to authenticate into your application using their Twitter   
account.

	- After successful login into Twitter account, **oauth token** and   
**oauth secret** are received.

	- After login into Twitter, user is redirected to your Android app again   
with the **oauth verifier**.

	- With received **oauth verifier** another request to get **access token**   
and **access token secret** is made.

	- As a result user inputs their message and tweet it from your application.

6. One way to obtain **OAuth** in Twitter is to use Twitter4J   
(as briefly described above). However Twitter itself uses **OAuth 1.0A**.   
You can obtain it by using **OAuth Echo**. More info could be found   
[here](https://docs.fabric.io/android/twitter/oauth-echo.html) and [here](https://dev.twitter.com/oauth/echo).

#SMS
1. In Android, you can send SMS using **SmsManager API** or devices   
**Built-in SMS** application.  

2. Since the introduction of Android 4.4, Google allows only one app to run   
as the **default SMS app**. Only the selected app is authorized to manage   
the SMS database entirely. The concept of one single default SMS app was   
introduced by Google and represents a change of the operating system that   
we are not able to influence.   

	In consideration of some apps that do not want to behave as the default  
 SMS app but still want to send messages, any app that has the `SEND_SMS`   
permission is still able to send SMS messages using **SmsManager**.   
If and only if an app is not selected as the default SMS app on Android 4.4,   
the system automatically writes the sent SMS messages to the SMS Provider   
(the default SMS app is always responsible for writing all sent messages   
to the SMS Provider).   

	However, if your app is designed to behave as the default SMS app,   
then while your app is not selected as the default, it's important that   
you understand the limitations placed upon your app and disable features   
as appropriate. Although the system writes sent SMS messages to the   
SMS Provider while your app is not the default SMS app, it does not write   
sent MMS messages and your app is not able to write to the SMS Provider   
for other operations, such as to mark messages as draft, mark them as read,   
delete them, etc.  

3. The **Built-in SMS application** solution is the easiest way, because you   
let the device handle everything for you. To send sms you have to simply   
create an Intent in your app, e.g.:

        Intent sendIntent = new Intent(Intent.ACTION_VIEW);  
	    sendIntent.putExtra("sms_body", "default content");  
	    sendIntent.setType("vnd.android-dir/mms-sms");  
	    startActivity(sendIntent);

4. Sending SMS via **SmsManager** needs **SEND_SMS** permission   
`<uses-permission android:name="android.permission.SEND_SMS" />` in your   
Android app. However, this permission protection level is dangerous,   
which means it might cover areas where the app wants data or resources that   
involve the user's private information, or could potentially affect the   
user's stored data or the operation of other apps. So it is not recommended   
to use that permission in app which isn't specifically a SMS application.  
To use the `SmsManager` you have to create: 

	- `SmsManager smsManager = SmsManager.getDefault();` and use its function:  
	- `public void sendTextMessage (String destinationAddress, String scAddress,  
String text, PendingIntent sentIntent, PendingIntent deliveryIntent)`. 
	
	- It's parameters:
		- *destinationAddress*: the address to send the message to,
		- *scAddress*: the service center address or `NULL` to use the current   
default SMSC,
		- *text*: the body of the message to send,
		- *sentIntent*:	if not `NULL` this `PendingIntent` is broadcast when   
the message is successfully sent, or failed. The result code will be   
`Activity.RESULT_OK` for success, or one of these errors:
			- `RESULT_ERROR_GENERIC_FAILURE`
			- `RESULT_ERROR_RADIO_OFF`
			- `RESULT_ERROR_NULL_PDU`
			- For `RESULT_ERROR_GENERIC_FAILURE` the sentIntent may include   
the extra `errorCode` containing a radio technology specific value,   
generally only useful for troubleshooting. The per-application based SMS   
control checks `sentIntent`. If `sentIntent` is `NULL` the caller will be   
checked against all unknown applications, which cause smaller   
number of SMS to be sent in checking period.
		- *deliveryIntent*:	if not `NULL` this `PendingIntent` is broadcast   
when the message is delivered to the recipient. The raw pdu of the status   
report is in the extended data (`"pdu"`).  


5. **Confirmation if share was successful/unsuccessful**: the   
confirmation is received from *deliveryIntent* in **SmsManager**.

6. **SDK level** for SMS is understood as Android SDK.

#Email
1. In Android to send an email, similarly to sending an SMS, you can use   
`Intent.ACTION_SEND` to call an **existing email client**. If no email   
clients are configured then Android system displays   
`No application can perform this action` error. This is the   
most straightforward and common use of sharing emails in Android.   
Here is the sample code to implement this type of sharing:

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        email.putExtra(Intent.EXTRA_TEXT, message); 

	Then it's needed to choose the email client with this code: 
  
	`startActivity(Intent.createChooser(email, "Select Email Client"));`  

	This will invoke the dialog with your existing email clients to select.
	
	However we have to remember that `Intent.ACTION_SEND` is designed   
to send overall data from one activity to another. So if we don't specify   
what apps should be in our chooser dialog, it might show all applications   
capable of sending our data. We can ensure that an Intent is handled only   
by an email app (and not other text messaging or social apps),   
by using the `ACTION_SENDTO` action and include the `mailto:` data scheme.   
But, this is rather not the solution we'd like to use because we have   
to specify a lot of logic by ourselves.  
	
2. To send an email without using built-in mailing apps you can use the   
[**JavaMail API**](https://code.google.com/archive/p/javamail-android/).  
This way you can send an email in the background without the user knowing.   
The application will do everything behind the scenes. Here is the [example](https://code.google.com/archive/p/javamail-android/)   
describing what should be done to send emails.   
Things to have in mind when creating an Android app with JavaMail API:
	- Download files from this [link](http://code.google.com/p/javamail-android/downloads/list)  
and add them as external libraries to your Android project,
	- JavaMail API is all defaulted to connect to   
the **Gmail (Google) SMTP server** and uses **Gmail authentication**,
	- Add permission `<uses-permission android:name="android.permission.INTERNET" />`  
in the Android app,
	- **Important note:** API is asking to have hardcoded username and   
password in code itself, which is a security risk. This is rather   
something that disqualifies this approach.

3. Other way to send emails in Android is using [**Gmail API**](https://developers.google.com/gmail/api/guides/sending).    

4. The **Gmail API** is a **RESTful API** that can be used to access   
**Gmail** mailboxes and send mails. For most mobile apps,   
the Gmail API is the best choice for authorized access   
to a user's Gmail data.  
The Gmail API gives you flexible, RESTful access to the user's inbox,   
with a natural interface to **Threads**, **Messages**, **Labels**,   
**Drafts**, and **History**. Your app can use the API to add Gmail   
features like:
	- Read messages from Gmail
	- Send email messages
	- Modify the labels applied to messages and threads
	- Search for specific messages and threads. 
5. Like other Google REST APIs, the Gmail API uses **OAuth 2.0**   
to handle authentication and authorization. Your app will specify one   
or more scopes: strings which identify resources that it needs to access.   
These scopes are used together with a set of tokens to secure a user's   
access to resources. A scope represents a particular form of access   
to a single resource or to a group of resources, for example:
	- Read a message from Gmail   
(https://www.googleapis.com/auth/gmail.readonly)
	- Change labels applied to a thread or message   
(https://www.googleapis.com/auth/gmail.modify)
	- Send a message on behalf of a user   
(https://www.googleapis.com/auth/gmail.compose)  

	More about authentication scopes can be read   
[here](https://developers.google.com/gmail/api/auth/scopes).

6. Prerequisites to create Android app that makes request to the Gmail   
are described [here](https://developers.google.com/gmail/api/quickstart/android).  
The most important things are:
	- Having Android SDK packages for **API 23 or later**,   
including the latest versions of **Google Repository**,   
**Android Support Library** and **Google Play Services**.
	- A Google account with Gmail enabled.
	- Registered application for Gmail API in   
**Google Developers Console**,  
(you can use [this wizard](https://console.developers.google.com/flows/enableapi?apiid=gmail)).
	- Added **OAuth 2.0 client ID** to credentials in   
Google Developers Console.
	- Minimum SDK of Android API version 11 or higher.
	- Following permissions added to your **AndroidManifest** file:
	
            <uses-permission android:name="android.permission.INTERNET" />
            <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
            <uses-permission android:name="android.permission.GET_ACCOUNTS" />
            <uses-permission android:name="android.permission.MANAGE_ACCOUNTS" />
            <uses-permission android:name="android.permission.USE_CREDENTIALS" />
            
7. Other important information:  
	- The Gmail API requires **MIME email messages** compliant   
with **RFC 2822** and encoded as **base64url strings**.
	- Creating an email message can be greatly simplified with   
the `MimeMessage` class in the `javax.mail.internet package`.
	- Once a message is created, it can be sent by supplying it   
in the request body of a call to `messages.send`.
		- `messages.send` requires **authorization** with   
at least one of the following scopes:
			- https://mail.google.com/
			- https://www.googleapis.com/auth/gmail.modify
			- https://www.googleapis.com/auth/gmail.compose
			- https://www.googleapis.com/auth/gmail.send
		- it supports an `/upload` URI and accepts uploaded media   
with the following characteristics:
			- Maximum file size: 35MB
			- Accepted Media MIME types: `message/rfc822`
		- More can be read [here](https://developers.google.com/gmail/api/v1/reference/users/messages/send#auth).

8. **Confirmation if share was successful/unsuccessful**: In **Gmail API**   
if send was successful, `messages.send` method returns   
a `Users.messages resource` in the response body. However it is  
hard to say from documentation if it gives us proper confirmation  
with required information.
9. **OAuth** can be obtained when using **Gmail API**. 
